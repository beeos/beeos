Set-Location kernel

if (!(Test-Path node_modules)) { 
  npm i
}

npm run webpack
npm run gulp

Copy-Item -r .\dist ..\

Set-Location ..
Set-Location programs

if (!(Test-Path node_modules)) { 
  npm i
}

npm run build

Copy-Item -r .\bin ..\dist

Set-Location ..
