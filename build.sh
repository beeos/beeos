# Move into the kernel directory
cd kernel

# If dependencies for build are not installed, install them
if [ ! -d "node_modules" ]; then
  npm i
fi

# Build the kernel and the website it will be served from
npm run webpack
npm run gulp

# Copy the built "binary" and webpage to the common dir
cp -r ./dist ../

# Move into the programs submodule
cd ../programs

# Install deps if they're not already
if [ ! -d "node_modules" ]; then
  npm i
fi

# Compile the typescript source into js
npm run build

# Copy "binaries" into the distribution folder
cp -r ./bin ../dist

# Return to original dir
cd ..
